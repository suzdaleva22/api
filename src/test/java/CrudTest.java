import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.example.entity.ListOfUsers;
import org.example.entity.entityfactory.UserFactory;
import org.example.utils.ConfigurationReader;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertEquals;

public class CrudTest {

    @BeforeTest
    public void setup() {
        RestAssured.baseURI = ConfigurationReader.get("baseUrl");
    }
    /**
     * // Метод для создания нового пользователя через API
     */
    @Test
    public void createNewUser() {

        given()
                .header("Content-Type", "application/json")
                .body(UserFactory.createUser())
                .log()
                .all()
                .when()
                .post( "/api/users")
                .then()
                .log()
                .all()
                .statusCode(201);
    }

    /**
     * // Метод для получения списка пользователей через API
     */
    @Test
    public void getListOfUsers() {
        ListOfUsers listOfUsers = given()
                .when()
                .get("/api/users?page=2")
                .then()
                .log()
                .all()
                .statusCode(SC_OK)
                .extract()
                .as(ListOfUsers.class);
        System.out.println(listOfUsers);
        assertThat(listOfUsers).isNotNull();
    }

    /**
     * // Метод для редактирования пользователя через API
     */
    @Test
    public void updateUserInfo() {
        System.out.println(UserFactory.createUser());
        Response response = given()
                .body(UserFactory.createUser())
                .log()
                .all()
                .when()
                .put("/api/users/2");

        response.then()
                .log()
                .all();

        assertThat(response.statusCode()).isEqualTo(200);
        assertThat(response.jsonPath().getString("updatedAt")).isNotNull();
    }
    /**
     * // Метод для удаления пользователя через API
     */
    @Test
    public void deleteUser() {
        Response response = RestAssured.given()
                .when()
                .delete("/api/users/2")
                .then()
                .extract()
                .response();

        assertEquals(response.statusCode(), 204);
    }

}

