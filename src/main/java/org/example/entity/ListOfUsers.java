package org.example.entity;

import java.util.List;

public record ListOfUsers (Integer page, Integer per_page, Integer total, Integer total_pages, List<UserDetails> data, Support support){

}