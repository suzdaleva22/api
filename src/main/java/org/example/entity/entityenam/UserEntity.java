package org.example.entity.entityenam;
/**
 * Enum `UserEntity` представляет возможные поля пользователя.
 *
 */
public enum UserEntity {

    NAME("name"),
    JOB("job");

    private final String fieldName;
    UserEntity(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}
