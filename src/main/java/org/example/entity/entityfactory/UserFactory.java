package org.example.entity.entityfactory;

import org.example.entity.User;
import org.example.entity.entityenam.UserEntity;

/**
 * Фабрика объектов User, которая используется для создания и изменения пользовательских объектов
 */
public class UserFactory {
    public static User createUser() {
        return new User(UserEntity.NAME.getFieldName(), UserEntity.JOB.getFieldName());
    }

    public static User changeUser() {
        return new User("morpheus", "zion resident");
    }
}